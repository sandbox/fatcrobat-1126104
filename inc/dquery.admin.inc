<?php

function dquery_admin_form($form_state){
    $form = array();
    
    $form['jquery'] =  array(
        '#type'             =>  'fieldset',
        '#title'            =>  t('jQuery settings'),
    );
    
    $form['jquery']['jquery_version'] = array(
        '#type'             =>  'radios',
        '#title'            =>  t('Select jQuery Version'),
        '#options'          =>  array(
            JQUERY_LATEST_VERSION   =>  t('jQuery Latest Version (@version)', array('@version' => JQUERY_LATEST_VERSION)),
            JQUERY_D6NMAX_VERSION   =>  t('jQuery Version for untouched Drupal (@version, no core patch required)', array('@version' => JQUERY_D6NMAX_VERSION)),
        ),
        '#default_value'    =>  variable_get('jquery_version', JQUERY_LATEST_VERSION)
    );
    
    $form['jquery']['jquery_ref'] = array(
        '#type'             =>  'radios',
        '#title'            =>  t('Choose how to load jQuery'),
        '#options'          =>  array(
            'GOOGLE'   =>  t('Load from Google CDN'),
            'LOCALE'   =>  t('Load from Local Server'),
        ),
        '#default_value'    =>  variable_get('jquery_ref', 'LOCALE')
    );
    
    $form = system_settings_form($form);
    return $form;
}